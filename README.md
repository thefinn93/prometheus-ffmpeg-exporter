# prometheus-ffmpeg-exporter
*a Prometheus exporter for long running ffmpeg proesses*

## Installing
Either download or [build](#Building) the latest version of the exporter, then
configure it to start as you would any other daemon.

An example systemd unit file is available in this repo at `prometheus-ffmpeg-exporter.service`.

## Using
1. [Install](#Installing) and start the exporter
2. Configure prometheus to scrape it. Metrics are served on port 9130 (by default) at `/metrics`:
```
  - job_name: ffmpeg
    static_configs:
      - targets: ['video-streamer:9130']
```
3. Run ffmpeg, passing the URL to the progress endpoint:
```
ffmpeg -i ... -progress http://video-streamer:9130/progress ...
```
If there are multiple ffmpeg instances, they must each submit a label to differentiate the two.
The only label available in the default configuration is `stream`:
```
ffmpeg -i ... -progress http://video-streamer:9130/progress?stream=example-a ...
ffmpeg -i ... -progress http://video-streamer:9130/progress?stream=example-b ...
```
Other labels may be used, but must be configured ahead of time. The label will appear
on all emitted metrics about that ffmpeg process.

## Building
Building should not require anything special, simply install the dependencies and build:

```
go get ./...
go build
```

## Configuration
While the default configuration may be sufficient, there are a number of options
that can be configured. A configuration file with all possible options and
their default values looks like this:

```json
{
  "measurable_keys": ["frame", "fps", "stream_0_0_q", "out_time_ms", "dup_frames", "drop_frames"],
  "unmeasurable_keys": ["progress", "bitrate", "total_size", "out_time_us", "out_time", "speed"],
  "ignore_unknown_keys": true,
  "bind": ":9130",
  "labels": ["stream"],
  "prometheus_namespace": "ffmpeg_status",
}
```

* **`measurable_keys`**: a list of keys that ffmpeg sends that should be converted into prometheus metrics.
* **`unmeasurable_keys`**: a list of keys that ffmpeg is expected to send but that are non numeric.
This can be used in conjunction with `ignore_unknown_keys` to alert on unexpected keys.
This is mostly used for debugging.
* **`ignore_unknown_keys`**: if set to `false`, keys received that are not listed
in the `measurable_keys` or `unmeasurable_keys` fields will be logged. This is mostly
used for debugging.
* **`bind`**: The port and optionally a host to bind to.
* **`labels`**: All possible labels that may be specified from ffmpeg via query parameters.
* **`prometheus_namespace`:** prefix to apply to all ffmpeg-related metrics.

To tell prometheus-ffmpeg-exporter to read a config file, specify the path to
the file as a command line argument:

```
prometheus-ffmpeg-exporter /etc/prometheus-ffmpeg-exporter.conf
```

If you are using the included systemd unit file, you will need to modify it accordingly.

## Bugs and Feature Requests
Open an issue, no guarantee that I'll look at it.

## Contributing
Pull requests welcome. If it's a big change maybe open an issue first to talk about it.
