package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

type Config struct {
	PrometheusNamespace string          `json:"prometheus_namespace"`
	MeasurableKeys      []string        `json:"measurable_keys"`
	IgnoreUnknownKeys   bool            `json:"ignore_unknown_keys"`
	UnmeasurableKeys    []string        `json:"unmeasurable_keys"`
	unmeasurableKeys    map[string]bool `json:"-"`
	Labels              []string        `json:"labels"`
	Bind                string          `json:"bind"`
}

var (
	config = Config{
		PrometheusNamespace: "ffmpeg_status",
		MeasurableKeys:      []string{"frame", "fps", "stream_0_0_q", "out_time_ms", "dup_frames", "drop_frames"},
		IgnoreUnknownKeys:   true,
		UnmeasurableKeys:    []string{"progress", "bitrate", "total_size", "out_time_us", "out_time", "speed"},
		Labels:              []string{"stream"},
		Bind:                ":9130",
	}
	metrics = make(map[string]*prometheus.GaugeVec)
)

func (c *Config) Load(filename string) {
	jsonFile, err := os.Open(filename)
	if err != nil {
		if !os.IsNotExist(err) {
			log.Println("Error opening config file", filename)
			panic(err)
		}
	}
	defer jsonFile.Close()
	byteValue, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		log.Println("Error reading config file", filename)
		panic(err)
	}
	json.Unmarshal(byteValue, &c)
	c.unmeasurableKeys = make(map[string]bool)
	for _, k := range c.UnmeasurableKeys {
		c.unmeasurableKeys[k] = true
	}
	log.Println("Successfully read config from", filename)
	log.Printf("%+v", c)
}

func initializeMetrics() {
	for _, key := range config.MeasurableKeys {
		metrics[key] = prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: config.PrometheusNamespace,
				Name:      key,
				Help:      fmt.Sprintf("The ffmpeg progress value for \"%s\"", key),
			},
			config.Labels,
		)
		log.Println("Registering metric", key)
		prometheus.MustRegister(metrics[key])
	}
}

func getLabels(q url.Values) prometheus.Labels {
	labels := make(prometheus.Labels)
	for _, key := range config.Labels {
		v := q.Get(key)
		if v == "" {
			continue
		}
		labels[key] = v
	}
	return labels
}

func progress(w http.ResponseWriter, r *http.Request) {
	labels := getLabels(r.URL.Query())
	log.Println("Progress streamer connected", labels)
	buf := make([]byte, 1024)
	defer func() {
		for _, metric := range metrics {
			metric.Delete(labels)
		}
	}()
	for {
		var n int
		n, err := r.Body.Read(buf)

		if err != nil {
			if err == io.EOF {
				log.Println("Request body closed", labels)
			} else {
				log.Println("Read error: ", err.Error(), labels)
			}
			return
		}

		for _, line := range strings.Split(string(buf[:n]), "\n") {
			parsed := strings.Split(line, "=")
			k := parsed[0]
			metric, ok := metrics[k]
			if !ok {
				if _, isKnown := config.unmeasurableKeys[k]; !isKnown && !config.IgnoreUnknownKeys && k != "" {
					log.Println("Unexpected unmeasurable key!", line)
				}
				continue
			}
			v, err := strconv.ParseFloat(parsed[1], 32)
			if err != nil {
				log.Println("Error parsing value", parsed[1], "for key", k, ":", err.Error())
				continue
			}
			metric.With(labels).Set(v)
		}
	}
}

func main() {
	for _, filename := range os.Args[1:] {
		config.Load(filename)
	}

	configJSON, err := json.Marshal(&config)
	if err != nil {
		panic(err)
	}

	log.Println("Loaded configuration:", string(configJSON))

	initializeMetrics()

	http.Handle("/metrics", promhttp.Handler())
	http.HandleFunc("/progress", progress)

	log.Println("Starting ffmpeg progress listener on", config.Bind)
	log.Fatal(http.ListenAndServe(config.Bind, nil))
}
